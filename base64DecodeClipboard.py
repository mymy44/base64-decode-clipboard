import pyperclip
import uuid
import base64
import os
from sys import platform
import tempfile
import subprocess

b64=pyperclip.paste()
if(b64[0]=="\""):
    b64=b64[1:]
if(b64[-1]=="\""):
    b64=b64[:-1]
b64Bytes=b64.encode('ascii')
decodedBytes=base64.b64decode(b64Bytes)
fileName=str(uuid.uuid4()) + ".pdf"
filePath=os.path.join(tempfile.gettempdir(),fileName)
with open(filePath, "wb") as pdf_file:
    pdf_file.write(decodedBytes)
if platform=="win32":
    #si = subprocess.STARTUPINFO()
    #si.dwFlags |= subprocess.STARTF_USESHOWWINDOW
    #si.wShowWindow = subprocess.SW_HIDE # default
    subprocess.call(f"SumatraPDF.exe {filePath}")
else:
    os.system(f"evince {filePath}")


